<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content animated fadeInRight">
		<div class="row">
			<div class="col-sm-12">
				<div class="ibox float-e-margins">
					<div class="ibox-title">
						<form class="form-inline form-index" action="#" method="post"
							id="form">
							<div class="form-group">
								<label class="control-label">操作员账号</label> <input
									name="userName" id="userName" type="text" class="form-control">
							</div>
							<div class="form-group">
								<label class="control-label">姓名</label> <input name="realName"
									id="realName" type="text" class="form-control">
							</div>
							<div class="form-group button-group">
								<html:auth res="admin/list">
									<input onclick="reload();" type="button" id="find"
										value="查询" class="btn btn-primary">
								</html:auth>
								<html:auth res="admin/add">
									<input type="button" id="new" value="新增"
										class="btn btn-primary"
										onClick="add();">
								</html:auth>
								<input onClick="resetClear();" type="reset" id="reset"
									value="重置" class="btn btn-primary">
							</div>
						</form>
					</div>
					<div class="ibox-content">
						<table id="data"
							class="table table-striped table-bordered table-hover dataTables-example">
							<thead>
								<tr>
									<th>账号</th>
									<th>姓名</th>
									<th>部门</th>
									<th>状态</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>

					</div>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="/WEB-INF/view/common.jsp"%>
	<script type="text/javascript">
		var table;
		function reload() {
			table.fnDraw();
		}
		$(document)
				.ready(
						function() {
							table = $("#data")
									.dataTable(
											{
												"bLengthChange": false, //改变每页显示数据数量 
												"searching": false,
												"bProcessing" : false, // 是否显示取数据时的那个等待提示
												"bServerSide" : true,//这个用来指明是通过服务端来取数据
												"sAjaxSource" : "admin/list",//这个是请求的地址
												"fnServerData" : retrieveData,
												"columns" : [ {
													"data" : "username",
													"defaultContent":""
												}, {
													"data" : "realname",
													"defaultContent":""
												}, {
													"data" : "department",
													"defaultContent":""
												}, {
													"data" : "status",
													"defaultContent":""
												} ],
												"fnServerParams" : function(aoData) {
													aoData.push({
																"name" : "userName",
																"value" : $("#userName").val()
															});
													aoData.push({
																"name" : "realName",
																"value" : $("#realName").val()
															});
												},
												"columnDefs" : [
														// 将name列变为红色
														{
															"targets" : [ 0 ], // 目标列位置，下标从0开始
															"render" : function(
																	data, type,
																	full) { // 返回自定义内容
																return "<span style='color:red;'>"
																		+ data
																		+ "</span>";
															}
														},
														// 增加一列，包括删除和修改，同时将需要传递的数据传递到链接中
														{
															"targets" : [ 4 ], // 目标列位置，下标从0开始
															"data" : "username", // 数据列名
															"render" : function(
																	data, type,
																	full) { // 返回自定义内容
																var modify = '<html:auth res="admin/modify">'
																		+ '<a href="#" onclick="modify(\''
																		+ data
																		+ '\')">修改</a></html:auth>';
																var del = '<html:auth res="admin/delete">'
																	+ '<a href="#" onclick="del(\''
																	+ data
																	+ '\')">删除</a></html:auth>';
																var resetPWD = '<html:auth res="admin/delete">'
																	+ '<a href="#" onclick="del(\''
																	+ data
																	+ '\')">删除</a></html:auth>';
																var resetPWD = '<html:auth res="admin/restPwd">'
																	+ '<a href="#" onclick="resetPwd(\''
																	+ data
																	+ '\')">重置密码</a></html:auth>';
																var unlock = '<html:auth res="admin/unlock">'
																	+ '<a href="#" onclick="unlock(\''
																	+ data
																	+ '\')">解锁</a></html:auth>';
																return del+"&nbsp;"+ modify+"&nbsp;"+ resetPWD+"&nbsp;"+ unlock;
															}
														} ]
											// 获取数据的处理函数
											});
						});

		// 3个参数的名字可以随便命名,但必须是3个参数,少一个都不行
		function retrieveData(url, data, fnCallback) {
			$.ajax({
				url : url,//这个就是请求地址对应sAjaxSource
				data : {
					"iDisplayLength" : data.iDisplayLength,
					"iDisplayStart" : data.iDisplayStart,
					"userName" : data.userName,
					"realname" : data.realName
				},//这个是把datatable的一些基本数据传给后台,比如起始位置,每页显示的行数
				type : 'post',
				dataType : 'json',
				async : false,
				success : function(result) {
					//console.log(JSON.stringify(result));
					fnCallback(result);//把返回的数据传给这个方法就可以了,datatable会自动绑定数据的
				},
				error : function(msg) {
				}
			});
		}
		function add(){
			var index = layer.open({
				  type: 2,
				  title:"操作员新增",
				  area: ['800px', '400px'],
				  fix: false, //不固定
				  maxmin: true,
				  content: 'admin/add'
				});
			layer.full(index);
		}
		function modify(id){
			var index = layer.open({
				  type: 2,
				  title:"操作员修改",
				  area: ['800px', '400px'],
				  fix: false, //不固定
				  maxmin: true,
				  content: 'admin/modify?id='+id
				});
			layer.full(index);
		}
		function del(id){
			//询问框
			layer.confirm('确定要删除么？', {
			  btn: ['确定','取消'] //按钮
			}, function(){
				$.ajax({
					url : "admin/delete",
					data : {
						id:id
					},
					type : 'post',
					contentType : 'application/x-www-form-urlencoded',
					encoding : 'UTF-8',
					cache : false,
					success : function(result) {
						if (result.success) {
							layer.alert('操作成功');
							reload();
						} else {
							layer.alert('操作失败:'+result.obj);
						}
					},
					error:function(){
						layer.alert('系统异常');
					}
				});
			}, function(){
			 //取消动作
			});
			
		}
		//解锁
		function unlock(id) {
			$.ajax({
				type : 'post',
				url : "admin/unlock",
				data : {
					id : id
				},
				cache : false,
				success : function(data) {
					if (data.success) {
						layer.alert('操作成功', {icon: 6},function(){
							layer.closeAll();
						});
					} else {
						layer.alert("操作失败"+data.obj);
					}
				},
				error : function() {
					layer.alert("系统错误");
				}
			});

		}
		//重置密码
		function resetPwd(id) {
			$.ajax({
				type : 'post',
				url : "admin/restPwd",
				data : {
					id : id
				},
				cache : false,
				success : function(data) {
					if (data.success) {
						layer.alert('操作成功', {icon: 6},function(){
							layer.closeAll();
						});
					} else {
						layer.alert("操作失败"+data.obj);
					}
				}
			});

		}
	</script>
</body>
</html>
