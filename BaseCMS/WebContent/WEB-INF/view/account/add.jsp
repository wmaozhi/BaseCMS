<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body>
	<html:add titles="公众号,公众号名称,appid,密钥,url地址,token,二维码"
		ids="account_required-input,name_required-input,appid_required-input,appsecret_required-input,url_none-txt,token_required-input,dubbleMa_none-img">
	<%-- 	<div class="item">
			<span class="preTitle">状态:</span><font color="red"></font> 
			<html:select cssClass="form-control" collection="articleStatus"  name="status" id="status">
			</html:select>
		</div> --%>
	</html:add>
</body>
</html>