<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<%@ page pageEncoding="UTF-8"%>
<script type="text/javascript" src="<c:url value="/js/pageJs/js/jquery.easyui.min.js"/>"></script>
<style>
.line {
	line-height: 30px;
}
</style>
	<div id="content">
		<div class="row" style="margin-left: auto;margin-right: auto;width: 95%">
			 <c:forEach items="${notices}" var="notice" varStatus="noti">
			 <div class="line">
				<a class="col-md-10" href="../notice/info?noticeId=${notice.id}" target="_blank"><span class="black">${notice.title}</span> 
					<c:if test="${noti.index <= 2 && pageNN == 1}">
				      <span class="red">NEW!</span> 
				   </c:if>
				</a>
				<span class="date col-md-2"><fmt:formatDate value="${notice.createDate}" pattern="yyyy-MM-dd" /></span>
			</div>
		    </c:forEach>
		</div>
	</div>
	<script type="text/javascript">
	$('#page').pagination({
		total : '${count}',
		pageSize:'${pageSize}',
		onSelectPage : function(pageNumber, pageSize) {
			queryPage(pageNumber, pageSize);
		}
	});
	</script>
