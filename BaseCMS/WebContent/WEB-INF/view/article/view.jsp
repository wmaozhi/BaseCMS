<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
</head>
<body>
	<div id="main"
		style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form class="form-inline form-index" id="form">
				<div class="item">
					<font color="red"></font><span class="preTitle">编码:</span> <input
						class="form-control" type="text" disabled name="id"
						value="${entity.id }">
				</div>
				<div id="uploadImg" class="item">
					<font color="red">*</font><span class="preTitle">公众号:</span> <input
						name="wxAccount" id="wxAccount" type="text"
						value="${entity.wxAccount }" disabled class="form-control" />
				</div>
				<div class="item">
					<font color="red"></font><span class="preTitle">栏目:</span> <select
						name="catgoryId" class="form-control" disabled>
						<c:forEach items="${catgory }" var="item">
							<option value="${item.id }"
								<c:if test="${item.id==entity.catgoryId }">selected = "selected"</c:if>>${item.name }</option>
						</c:forEach>
					</select>
				</div>
				<div class="item">
					<font color="red"></font><span class="preTitle">标题:</span> <input
						name="title" id="title" value="${entity.title }" disabled
						type="text" class="form-control" />
				</div>

				<div class="item">
					<font color="white"></font> <span class="preTitle">摘要:</span> <input
						name="remark" id="remark" value="${entity.remark }" disabled
						type="text" class="form-control" />
				</div>
				<div class="item">
					<font color="red"></font> <span class="preTitle">状态:</span>
					<html:select cssClass="form-control" collection="articleStatus"
						selectValue="${entity.status }" name="status" disabled="disabled"
						id="status">
					</html:select>
				</div>
				<div class="item">
					<font color="white"></font> <span class="preTitle">正文:</span>
					<!--style给定宽度可以影响编辑器的最终宽度-->
					<div class="upImg">
						<script type="text/plain" id="container" name="content" disabled
							style="width:1000px;height:240px;">${entity.content }</script>
					</div>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		//实例化编辑器
		var ue = UE.getEditor('container');
		ue.ready(function() {
			setDisabled();
		});
		function setDisabled() {
			UE.getEditor('container').setDisabled('fullscreen');
		}
		function enableBtn() {
			var div = document.getElementById('btns');
			var btns = UE.dom.domUtils.getElementsByTagName(div, "button");
			for (var i = 0, btn; btn = btns[i++];) {
				UE.dom.domUtils.removeAttributes(btn, [ "disabled" ]);
			}
		}
	</script>
</body>
</html>