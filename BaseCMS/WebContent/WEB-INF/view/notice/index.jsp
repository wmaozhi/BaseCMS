<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/header.jsp"%>
</head>
<body>
<%@ include file="/WEB-INF/view/common.jsp"%>
	<div id="main" style="margin-left: auto; margin-right: auto;">

		<div class="form-container" id="search">
			<div>
				<span class="title">公共服务</span> <img
					src="<c:url value="/images/jiantou.png" />" /> <span
					class="sec-title">公告管理</span>
			</div>
			<hr />
			<form class="form-inline form-index" action="#" method="post"
				id="form">
				<div class="form-group">
					<label class="control-label">标题</label> <input name="title"
						id="title" type="text" maxlength="20" class="form-control" />
				</div>
				<div class="form-group">
					<label class="control-label">公告类型</label> 
					 <html:select cssClass="form-control" id="type" name="type" collection="noticeType" selectValue="">
										<option value="">-请选择-</option>	
						</html:select>
				</div>
				<div class="form-group">
					<label class="control-label">状态</label> <select name="state"
						id="state" class="form-control">
						<option value="">--全部--</option>
						<c:forEach items="${STATEMAP }" var="map">
							<option value="${map.key}">${map.value}</option>
						</c:forEach>
					</select>
				</div>
				<div class="form-group">
					<label class="control-label">创建时间</label> <input name="startDate"
						id="startDate" type="text" class="form-control"
						onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})"
						style="width: 170px;" /> - <input name="endDate" id="endDate"
						type="text" class="form-control"
						onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})"
						style="width: 170px;" />
				</div>

				<div class="form-group button-group" style="width: 100%;">

					<html:auth res="/notice/list">
						<input onclick="queryPage();" type="button" id="search" value="查询"
							class="btn btn-default">
					</html:auth>
					<input onClick="resetClear();" type="reset" id="reset" value="重置"
						class="btn btn-default">
					<html:auth res="/notice/add">
						<input type="button" id="new" value="新增" class="btn btn-default"
							onClick="addNotice();">
					</html:auth>
				</div>
			</form>
		</div>
		<div class="table-container" id="content"></div>
		<div style="text-align: center;">
			<div id="page" class="easyui-pagination"
				data-Options="pageList:[15,30,50,100],pageSize:15"
				style="background: #efefef; border: 1px solid #ccc; width: 90%; margin: 0;"></div>
		</div>
		<div id="over" class="over">
			<div id="layout" class="layout">
				<img src="<c:url value="/images/loaderc.gif"/>" />
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			queryPage();
		});
		$.fn.datebox.defaults.formatter = function(date) {
			var y = date.getFullYear();
			var m = date.getMonth() + 1;
			var d = date.getDate();
			return y + '-' + m + '-' + d;
		};
		function queryPage(pageNo, pageSize) {
			if ($('#startDate').val() != ''
					&& $('#endDate').val() != '') {
				var date1 = new Date($('#startDate').val()
						.replace("-", "/").replace("-", "/"));
				var date2 = new Date($('#endDate').val().replace(
						"-", "/").replace("-", "/"));
				if (date1 > date2) {
					$("#alert-content").html("创建开始时间不能大于结束时间");
					$("#alert-modal").modal("show");
					$("#over").css("display", "none");
					$("#layout").css("display", "none");
					return;
				}
			}
			showLoading();
			$.ajax({
				url : "notice/list",
				type : "post",
				data : {
					pageNo : pageNo,
					pageSize : pageSize,
					title : $('#title').val(),
					status : $('#state').val(),
					type : $('#type').val(),
					createDateStart : $('#startDate').val(),
					createDateEnd : $('#endDate').val()
				},
				cache : false,
				success : function(html) {
					$("#over").css("display", "none");
					$("#layout").css("display", "none");
					$('#content').replaceWith(html);
				},
				error : function() {
					$("#alert-content").html("您没有该权限,请联系管理员");
					$("#alert-modal").modal("show");
				}
			});
		};
		function showLoading() {
			document.getElementById("over").style.display = "block";
			document.getElementById("layout").style.display = "block";
		}
		function addNotice(pageNo, pageSize) {
			$.ajax({
				url : "notice/add",
				type : "get",
				data : {
					pageNo : pageNo,
					pageSize : pageSize,
					riskLevel : $('#riskLevel').val()
				},
				cache : false,
				success : function(html) {
					$('#main').replaceWith(html);
				},
				error : function() {
					$("#alert-content").html("您没有该权限,请联系管理员");
					$("#alert-modal").modal("show");
				}
			});
		};
		function isDel(id, pageNo, pageSize) {
			$("#confirm-content").html("确认要作废？");
			$("#confirm-ok").unbind("click");
			$("#confirm-ok").bind("click",function(){
				deleteLogo(id, pageNo, pageSize);
			});
			$("#confirm-modal").modal("show");
		}
		function deleteLogo(id, pageNo, pageSize) {
			$.ajax({
				url : "notice/cancle",
				type : "POST",
				data : {
					pageNo : pageNo,
					pageSize : pageSize,
					id : id
				},
				contentType : 'application/x-www-form-urlencoded',
				encoding : 'UTF-8',
				cache : false,
				success : function(result) {
					if (result.success) {
						$("#alert-content").html("作废成功");
						$("#alert-ok").bind("click",function(){
							window.location.href = "notice";
						});
						$("#alert-modal").modal("show");
					} else {
						$("#alert-content").html(result.message);
						$("#alert-modal").modal("show");
					}
				},
				/* error : function() {
					$.messager.alert("提示", result.message);
				} */
				error : function() {
					$("#alert-content").html("您没有该权限,请联系管理员");
					$("#alert-modal").modal("show");
				}
			});
		};
		//重置，清空datebox
		function resetClear() {
			$('#startDate').datebox('setValue', '');
			$('#endDate').datebox('setValue', '');
		}
	</script>
</body>
</html>