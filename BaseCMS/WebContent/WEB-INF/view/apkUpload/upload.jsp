<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/header.jsp"%>
<link
	href="<c:url value="/js/umeditor/themes/default/css/umeditor.css"/>"
	type="text/css" rel="stylesheet">
<script type="text/javascript">
	window.UMEDITOR_HOME_URL = "../js/umeditor/";
</script>

<script type="text/javascript" charset="utf-8"
	src="<c:url value="/js/umeditor/umeditor.config.js"/>"></script>
<script type="text/javascript" charset="utf-8"
	src="<c:url value="/js/umeditor/umeditor.min.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/umeditor/lang/zh-cn/zh-cn.js"/>"></script>
<script type="text/javascript"
	src="<c:url value="/js/jquery.blockUI.min.js"/>"></script>
<style>
.table-bordered {
	text-align: left;
}

.table-bordered>tbody tr:nth-child(odd) {
	background: #fff;
}

.table-bordered>tbody tr td {
	padding-left: 40px;
	line-height: 30px;
}

.preTitle {
	width: 80px;
	display: -moz-inline-box;
	display: inline-block;
}
</style>
</head>
<body>
	<%@ include file="/WEB-INF/view/common.jsp"%>
	<div class="form-container">
		<div>
			<span class="title">apk上传</span> <span style="float: right;"><input
				type="button" onclick="history.go(-1)" value="返回"
				class="btn btn-default"></span>
		</div>
		<hr />
	</div>
	<div id="main"
		style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form action="notice/add" name="addNoticeForm" id="addNoticeForm"
				method="post" enctype="multipart/form-data"
				class="form-inline form-index">

				<div id="uploadImg" style="margin: 15px;">
					<font color="red">*</font><span class="preTitle">apk文件:</span> <input
						type="file" id="upApk" name="myfile" onchange="ajaxFileUpload();" />
						<a id="result" href=""></a>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">

		function ajaxFileUpload() {
			//开始上传文件时显示一个图片,文件上传完成将图片隐藏
			//$("#loading").ajaxStart(function(){$(this).show();}).ajaxComplete(function(){$(this).hide();});
			//执行上传文件操作的函数
			$('#uploadImg').block({
				message : '<h2>正在上传...</h2>'
			});
			$.ajaxFileUpload({
						//处理文件上传操作的服务器端地址(可以传参数,已亲测可用)
						url : '../common/apkUpload',
						secureuri : false, //是否启用安全提交,默认为false
						fileElementId : 'upApk', //文件选择框的id属性
						dataType : 'json', //服务器返回的格式,可以是json或xml等
						success : function(data, status) { //服务器响应成功时的处理函数
							$('#uploadImg').unblock();
						},
						error : function(data, status, e) { //服务器响应失败时的处理函数
							$('#uploadImg').unblock();
						}
					});
		}
	</script>
</body>
</html>