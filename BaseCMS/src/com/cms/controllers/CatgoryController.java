package com.cms.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cms.entity.Catgory;
import com.cms.service.CatgoryService;
import com.cms.service.DataDictService;
import com.cms.support.Result;
import com.cms.support.StringEditor;

/**
 * @ClassName: CatgoryController
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangp
 * @date 2016年4月8日 上午11:31:00
 * 
 */
@Controller
public class CatgoryController {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CatgoryController.class);

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat(
				"yyyy-MM-dd HH:mm:ss");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
		binder.registerCustomEditor(String.class, new StringEditor());
	}

	@Autowired
	private CatgoryService catgoryService;

	
	@Autowired
	private DataDictService dataDictService;
	
	/**
	 * @Title: loadDateDict
	 * @Description: TODO(每次请求之前都会调用)
	 * @param model
	 * @throws
	 */
	@ModelAttribute
	public void loadDateDict(Model model){
		model.addAttribute("dataDict", JSONArray.toJSON(dataDictService.listDataByName(null)).toString());
	}
	/**
	 * @Title: index
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param model
	 * @return String
	 * @throws
	 */
	@RequestMapping("/catgory")
	public String index(Model model) {
		return "catgory/index";
	}

	/**
	 * @Title: list
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param name
	 * @param description
	 * @param iDisplayLength
	 * @param iDisplayStart
	 * @param sEcho
	 * @param model
	 * @return Object
	 * @throws
	 */
	@ResponseBody
	@RequestMapping("/catgory/list")
	public Object list(String wxAccount,String name, String status, String description,
			Integer iDisplayLength, Integer iDisplayStart, Integer sEcho,
			Model model) {
		try {

			Long totalCount = catgoryService
					.getCount(wxAccount,name, status, description);
			List<Catgory> list = catgoryService.list(wxAccount,name, status, description,
					iDisplayStart, iDisplayLength);
			JSONObject getObj = new JSONObject();
			getObj.put("sEcho", sEcho);// 不知道这个值有什么用,有知道的请告知一下
			getObj.put("iTotalRecords", totalCount);// 实际的行数
			getObj.put("iTotalDisplayRecords", totalCount);// 显示的行数,这个要和上面写的一样
			getObj.put("aaData", list);// 要以JSON格式返回
			return getObj;
		} catch (Exception e) {
			LOGGER.error(e.toString());
			return null;
		}
	}

	/**
	 * @Title: add
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param model
	 * @return String
	 * @throws
	 */
	@RequestMapping(value = "/catgory/add", method = RequestMethod.GET)
	public String add(Model model) {
		return "catgory/add";
	}

	/**
	 * @Title: doAdd
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param entity
	 * @param model
	 * @return Object
	 * @throws
	 */
	@ResponseBody
	@RequestMapping(value = "/catgory/add", method = RequestMethod.POST)
	public Object doAdd(Catgory entity, Model model) {
		try {
			entity.setModifyDate(new Date());
			entity.setStatus("1");
			catgoryService.add(entity);
		} catch (Exception e) {
			LOGGER.error("doAdd exception：", e);
			return new Result(false, "系统异常,请稍后再试");
		}
		return new Result();
	}

	/**
	 * @Title: modify
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param model
	 * @param id
	 * @return String
	 * @throws
	 */
	@RequestMapping(value = "/catgory/modify", method = RequestMethod.GET)
	public String modify(Model model, Integer id) {
		model.addAttribute("entity", catgoryService.loadById(id));
		return "catgory/modify";
	}

	/**
	 * @Title: modify
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param entity
	 * @param model
	 * @return Object
	 * @throws
	 */
	@ResponseBody
	@RequestMapping(value = "/catgory/modify", method = RequestMethod.POST)
	public Object modify(Catgory entity, Model model) {
		try {
			entity.setModifyDate(new Date());
			catgoryService.modify(entity);
		} catch (Exception e) {
			LOGGER.error("modify exception,", e);
			return new Result(false, "系统异常,请稍后再试");
		}
		return new Result();
	}

	/**
	 * @Title: delete
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param model
	 * @param id
	 * @return Object
	 * @throws
	 */
	@ResponseBody
	@RequestMapping(value = "/catgory/delete")
	public Object delete(Model model, @RequestParam Integer id) {
		try {
			catgoryService.delete(id);
		} catch (Exception e) {
			LOGGER.error("delete exception:", e);
			return new Result(false, "系统异常,请稍后再试");
		}
		return new Result();
	}

	/**
	 * @Title: view
	 * @Description: TODO(这里用一句话描述这个方法的作用)
	 * @param model
	 * @param id
	 * @return Object
	 * @throws
	 */
	@RequestMapping(value = "/catgory/view")
	public String view(Model model, @RequestParam Integer id) {
		model.addAttribute("entity", catgoryService.loadById(id));
		return "catgory/view";
	}

}
