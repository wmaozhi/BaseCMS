package com.cms.entity.weixin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

import com.cms.common.hibernate.BaseEntity;

/**
 * 微信公众账号
 */
@SuppressWarnings("serial")
@Entity(name="t_weixin_account")
public class Account extends BaseEntity {
	@Column(nullable = false, unique = true)
	private String account;// 账号
	@Column
	private String appid;// appid
	@Column
	private String appsecret;// appsecret
	@Column
	private String url;// 验证时用的url
	@Column
	private String token;// token

	// ext
	@Column
	private Integer msgcount;// 自动回复消息条数;默认是5条
	@Column
	private String name;
	@Column
	private String dubbleMa;
	@Transient
	private String name_like;// 名称


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName_like() {
		return name_like;
	}

	public void setName_like(String name_like) {
		this.name_like = name_like;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getAppsecret() {
		return appsecret;
	}

	public void setAppsecret(String appsecret) {
		this.appsecret = appsecret;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Integer getMsgcount() {
		return msgcount;
	}

	public void setMsgcount(Integer msgcount) {
		this.msgcount = msgcount;
	}

	public String getDubbleMa() {
		return dubbleMa;
	}

	public void setDubbleMa(String dubbleMa) {
		this.dubbleMa = dubbleMa;
	}

}