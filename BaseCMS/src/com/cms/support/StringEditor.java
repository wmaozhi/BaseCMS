package com.cms.support;

import org.springframework.beans.propertyeditors.PropertiesEditor;

public class StringEditor extends PropertiesEditor {    
    @Override    
    public void setAsText(String text) throws IllegalArgumentException {    
        if (text.equals("")) {    
            setValue(null);    
        }else {
			setValue(text);
		}    
    }    
    
    @Override    
    public String getAsText() {    
        return getValue().toString();    
    }    
}  