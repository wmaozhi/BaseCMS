package com.cms.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cms.entity.Article;
import com.cms.service.ArticleService;

@Service
public class ArticleServiceImpl extends BaseServiceImpl implements
		ArticleService {

	@Transactional
	@Override
	public Article loadById(Integer id) {
		return baseDao.get(Article.class, id);
	}

	@Transactional
	@Override
	public List<Article> list(String wxAccount,String title, String status,String remark,String content,Integer catgoryId, Date startDate,Date endDate,
			Integer from, Integer to) {
		// 查询结果
		List<Object> any = new ArrayList<Object>();
		any.add(Transformers.aliasToBean(Article.class));

		if (StringUtils.isNotBlank(title)) {
			any.add(Restrictions.like("title", title.trim(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(remark)) {
			any.add(Restrictions.like("remark", remark.trim(),
					MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(content)) {
			any.add(Restrictions.like("content", content.trim(),
					MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(wxAccount)) {
			any.add(Restrictions.like("wxAccount", wxAccount.trim(),
					MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(status)) {
			any.add(Restrictions.eq("status", status.trim()));
		}
		if (catgoryId!=null) {
			any.add(Restrictions.eq("catgoryId", catgoryId));
		}
		if (startDate!=null) {
			any.add(Restrictions.ge("modifyDate", startDate));
		}
		if (endDate!=null) {
			any.add(Restrictions.le("modifyDate", endDate));
		}
		any.add(Projections.property("id").as("id"));
		any.add(Projections.property("title").as("title"));
		any.add(Projections.property("remark").as("remark"));
		any.add(Projections.property("content").as("content"));
		any.add(Projections.property("catgoryId").as("catgoryId"));
		any.add(Projections.property("showOrder").as("showOrder"));
		any.add(Projections.property("status").as("status"));
		any.add(Projections.property("wxAccount").as("wxAccount"));
		any.add(Projections.property("modifyDate").as("modifyDate"));

		List<Article> entity = baseDao.findByAny(Article.class, from, to,
				any.toArray());
		return entity;
	}

	@Transactional
	@Override
	public Long getCount(String wxAccount,String title, String status,String remark,String content,Integer catgoryId, Date startDate,Date endDate) {
		// 查询结果
		List<Criterion> any = new ArrayList<Criterion>();
		// 查询条件
		if (StringUtils.isNotBlank(title)) {
			any.add(Restrictions.like("title", title.trim(), MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(remark)) {
			any.add(Restrictions.like("remark", remark.trim(),
					MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(content)) {
			any.add(Restrictions.like("content", content.trim(),
					MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(wxAccount)) {
			any.add(Restrictions.like("wxAccount", wxAccount.trim(),
					MatchMode.ANYWHERE));
		}
		if (StringUtils.isNotBlank(status)) {
			any.add(Restrictions.eq("status", status.trim()));
		}
		if (catgoryId!=null) {
			any.add(Restrictions.eq("catgoryId", catgoryId));
		}
		if (startDate!=null) {
			any.add(Restrictions.ge("modifyDate", startDate));
		}
		if (endDate!=null) {
			any.add(Restrictions.le("modifyDate", endDate));
		}
		return baseDao.countByAny(Article.class,
				any.toArray(new Criterion[any.size()]));
	}

	@Transactional
	@Override
	public Article add(Article entity) {
		 baseDao.save(entity);
		 return entity;
	}

	@Transactional
	@Override
	public boolean modify(Article entity) {
		try {
			baseDao.update(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Transactional
	@Override
	public boolean delete(Integer id) {
		try {
			Article entity = new Article();
			entity.setId(id);
			baseDao.delete(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
